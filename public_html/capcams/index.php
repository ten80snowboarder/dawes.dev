<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>CAPCAMS</title>
	<link href="/capcams/library/css/style.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
</head>
<body>

	<div class="wrap">

		<h1>CAPCAMS</h1>
		<p><?php echo time(); ?></p>

		<img src="https://ien.dot.ny.gov/ien/region01/cctv/cam2001.jpg" />
		<img src="https://ien.dot.ny.gov/ien/region01/cctv/cam2002.jpg" />
		<img src="https://ien.dot.ny.gov/ien/region01/cctv/cam2003.jpg" />
		<img src="https://ien.dot.ny.gov/ien/region01/cctv/cam2004.jpg" />
		<img src="https://ien.dot.ny.gov/ien/region01/cctv/cam2005.jpg" />
		<img src="https://ien.dot.ny.gov/ien/region01/cctv/cam2006.jpg" />

	</div>

	<script src="/capcams/library/js/scripts.js"></script>

</body>
</html>